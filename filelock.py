"""File lock to ensure only one instance of the script can be run at the same time

Found here:
http://amix.dk/blog/post/19531"""
import os, sys
from contextlib import contextmanager

@contextmanager
def file_lock(lock_file):
    """Generator function providing file lock functionality"""
    if os.path.exists(lock_file):
        print 'Only one script can run at once. '\
            'Script is locked with %s' % lock_file
        sys.exit(-1)
    else:
        open(lock_file, 'w').write("1")
        try:
            yield
        finally:
            os.remove(lock_file)