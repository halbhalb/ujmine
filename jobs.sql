--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `html` text,
  `description` varchar(25000) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `apply_url` varchar(255) DEFAULT NULL,
  `salary` varchar(255) DEFAULT NULL,
  `id` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `industries` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `career_level` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `application` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`job_id`),
  UNIQUE KEY `id` (`id`)
);
