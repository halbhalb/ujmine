Collect and store locally jobs from Universal Jobmatch
======================================================

Simple Python scripts to scrape and process job postings from Universal Jobmatch.



Requirements
------------

Python 2.x, MySQL 5.x, Python MySQL buindings, `unidecode`, `requests` and `pyquery` Python packages.


Installing and running
----------------------

There are no configuration files. 

You will need a database with a single table called `jobs`. `jobs.sql` contains schema for that table. You will need to tell the scripts how to connect to that database. This is done in the scripts directly. 

`mine.py` and `daily-mine.py` are the scripts you can run. You need to edit them and chnage the database connection details there.