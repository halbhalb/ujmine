"""Scrap and process jobs postings from Universal Jobmatch."""
from unidecode import unidecode
from pyquery import PyQuery as pq
from abc import ABCMeta, abstractmethod
from datetime import datetime
from copy import copy
import requests
import MySQLdb

class JobNotFound(Exception):
	"""Exception thrown if a job is not found on the site."""
	pass

class Job:
	"""Class to store all job data as well as parse HTML page and retrieve the data from it."""

	@staticmethod
	def get_uj_url(id):
		"""Build the url for a job page on Universal Jobmatch for a given numeric job ID."""
		job_url = 'https://jobsearch.direct.gov.uk/GetJob.aspx?JobID=' + str(id)
		return job_url

	def __init__(self, html):
		"""Create an instance and populate it with job data parsed from the HTML string given."""
		self.html = unidecode(html.decode('utf8'))
		self.parse_html(self.html)

	def parse_html(self, html):
		"""Parse given HTML string and populate use the data from it to populate the object."""
		self.pq = pq(html)
		self.description =  self.pq('div.jobViewContent div.jobDescription').text()
		self.description = unidecode(self.description) if isinstance(self.description, unicode) else self.description 
		self.company =  self.pq('div.jobViewContent h2:first').text()
		self.company = unidecode(self.company) if isinstance(self.company, unicode) else self.company 
		self.title =  self.pq('div.jobViewContent h2:first').nextAll('h2').text()
		self.title = unidecode(self.title) if isinstance(self.title, unicode) else self.title 
		self.apply_url = self.pq('div.jobViewContent div.buttonWrapper a').attr('href')
		self.apply_url = unidecode(self.apply_url) if isinstance(self.apply_url, unicode) else self.apply_url 
		summary_parts = [(unidecode(e.text) if isinstance(e.text, unicode) else e.text) for e in self.pq('div.jobViewSummary dl').children()]
		summary_dict = {summary_parts[i]:summary_parts[i+1] for i in range(len(summary_parts)-1)}
		self.salary = summary_dict['Salary']  if 'Salary' in summary_dict else None
		self.id = summary_dict['Job ID'] if 'Job ID' in summary_dict else None
		self.date = datetime.strptime(summary_dict['Posting Date'],'%d/%m/%Y') if 'Posting Date' in summary_dict else None
		self.location = summary_dict['Location'] if 'Location' in summary_dict else None
		self.industries = summary_dict['Industries'] if 'Industries' in summary_dict else None
		self.type = summary_dict['Job type'] if 'Job type' in summary_dict else None
		self.career_level = summary_dict['Career level'] if 'Career level' in summary_dict else None
		self.reference = summary_dict['Job reference code'] if 'Job reference code' in summary_dict else None
		self.application = summary_dict['Application methods'] if 'Application methods' in summary_dict else None

class Miner:
	"""Collect job data from Universal Jobmatch and pass it for further processing."""

	def get_job(self, job_id):
		"""Get a single job from the site using it's ID and parse to a Job object."""
		#job wit no aply button
		#job_id = 6718375
		job_url = Job.get_uj_url(job_id)
		r = requests.get(job_url)
		if (200 != r.status_code) or (r.url.find('Error.aspx') > 0):
			raise JobNotFound
		job_object = Job(unidecode(r.text))
		return job_object

	def process_jobs(self, ids, processor):	
		"""Get all jobs with IDs from given list and pass each one to the given processor."""
		for job_id in ids:
			try:
				job_object = self.get_job(job_id)
				processor.process_job(job_object)
			except JobNotFound:
				pass


class Processor:
	"""Abstract job processor class."""
	__metaclass__ = ABCMeta

	@abstractmethod
	def process_job(self, job): 
		"""Process a single job - abstract method to be implemented by concrete subclasses."""
		pass


class DbProcessor(Processor):
	"""Job processor which saves jobs to MySQL database."""
	fields = [ 'html', 'description', 'company', 'title', 'apply_url', 
		'salary', 'id', 'date', 'location', 'industries', 'type', 'career_level', 
		'reference', 'application']
	table = 'jobs'

	def __init__(self, connection):
		"""Create a processor using given database connection."""
		self.connection = connection
		self.cursor = self.connection.cursor() 

	def process_job(self, job):
		"""Save the given job to the database.

		If a job with the same Universal Jobmatch ID is already in the database, 
		the new job will not be saved.
		"""
		try:
			print str(job.id) + ': ' + job.title
			values = copy(job.__dict__)
			values['date'] = values['date'].strftime('%Y-%m-%d') if values['date'] else None
			query = 'INSERT INTO `'+self.table+'` ('+\
				', '.join(['`'+f+'`' for f in self.fields] )+')'+\
				' VALUES ('+\
				', '.join(['NULL' if values[f] is None else "'"+MySQLdb.escape_string(values[f])+"'" for f in self.fields] )+');'
			self.cursor.execute(query)
		except MySQLdb.IntegrityError:
			print "job already in the database"
	
	def find_latest_job(self):
		"""Find the largest Universal Jobamtch ID among the jobs saved in the database."""
		query = "SELECT MAX(id) FROM `"+self.table+"`"
		self.cursor.execute(query)
		result =  self.cursor.fetchone()
		return int(result[0])
