#!/usr/bin/python
# Script to collect jobs from Universal JObmatch specified by a range of IDs 
# and save them in a database.
from unidecode import unidecode
from ujmine import *
import MySQLdb

# connect to a database
connection = MySQLdb.connect(host="localhost", user="ujmine", passwd="ujmine", db="ujmine")
connection.autocommit(True)

# create a database saving processor using that connection
processor = DbProcessor(connection)

miner = Miner()

# specify the range of IDs for jobs to be retrieved
end_job_id = 6883472
start_job_id = end_job_id - 100000
# tell miner to get all those jobs and process them using the processor which 
# will save them to the database
miner.process_jobs(reversed(range(start_job_id,end_job_id)), processor)

connection.close