#!/usr/bin/python
# A bit more useulf script to collect jobs and store them in a database.
# Checks the jobs currently in the database and finds the highest ID.
# Then it attempts to collect a given number of jobs starting from that highest ID + 1 
# and going up.
# This can be run regularly, for example as a cron script to collect all jobs
# posted on Universal Jobmatch during that time.
from unidecode import unidecode
from ujmine import *
from filelock import file_lock
import MySQLdb


#specifyu the number of jobs to collect in a single run
how_many_jobs = 1000

#obtain a file lock to prevent more than one instance running simultaneously
with file_lock('/tmp/uj-mine-lock.tmp'):
	# connect to a database
	connection = MySQLdb.connect(host="localhost", user="ujmine", passwd="ujmine", db="ujmine")
	connection.autocommit(True)

	# create a database saving processor using that connection
	processor = DbProcessor(connection)

	# specify the range of IDs for jobs to be retrieved
	# starting from the current highest plus one
	miner = Miner()
	start_job_id = processor.find_latest_job()+1
	end_job_id = start_job_id + how_many_jobs

	# tell miner to get all those jobs and process them using the processor which 
	# will save them to the database
	miner.process_jobs(range(start_job_id,end_job_id), processor)

	connection.close